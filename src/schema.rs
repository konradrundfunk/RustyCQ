table! {
    cq (id) {
        id -> Nullable<Integer>,
        call -> Text,
        time -> Timestamp,
        frequency -> Float,
        mode -> Text,
        name -> Nullable<Text>,
    }
}
