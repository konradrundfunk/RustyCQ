extern crate clap;
extern crate diesel;


use chrono::Utc;
use clap::{App, Arg, SubCommand};
use diesel::{Connection, IntoSql, SqliteConnection};
use diesel::sql_types::Time;


struct Database {
    connection: SqliteConnection,
    file: String,
}

impl Database {
    pub fn connect(file:String) -> (SqliteConnection, String) {
        (SqliteConnection::establish(&file).expect("Error connecting."), file)
    }
}


pub struct QsoSimple {
    callsign: String,
    frequency: String,
    mode: String,
    time: String,
    name: String,
}

impl QsoSimple {

    fn write(&self, db_credentials: Database){
        /*cq.filter(published.eq(true))
            .limit(5)
            .load::<cq>(&connection)
            .expect("Error loading posts");*/
    }
    fn read(db_credentials: Database){

    }

}

// #[derive(Queryable)]
pub struct QsoSimpDb {
    pub id: i32,
    pub call:String,
    pub time:String,
    pub frequency: f64,
    pub mode: String,
    pub name: String,
}



pub fn cli() {
    let utctime  = String::from(Utc::now().to_string());
    let cli = App::new("rustycq")
        .about("A simple cli logbook.")
        .version("0.0.1")
        .author("Konradrundfunk")
        .arg(Arg::with_name("call")
            .long("callsign")
            .help("Enter the call of the other person")
            .short("c")
            .takes_value(true)
            .required(true)
        )
        .arg(Arg::with_name("time")
            .long("time")
            .help("Time in UTC if no value is passed set current time.")
            .short("ti")
            .takes_value(true)
            .required(true)
            .default_value(&utctime)
        )
        .arg(Arg::with_name("freq")
            .long("frequency")
            .help("The frequency*ies(separated by ,) that you chatted on.")
            .short("f")
            .takes_value(true)
            .required(true)
        )
        .arg(Arg::with_name("mode")
            .long("mode")
            .help("The mode you worked in.")
            .short("m")
            .takes_value(true)
        )
        .arg(Arg::with_name("op name")
            .long("name")
            .help("The name of the Operator you spoke with..")
            .short("n")
            .takes_value(true)
        )
    .get_matches();

    let data = QsoSimple {
        mode: String::from(cli.value_of("mode").unwrap_or("Please specify a mode, or select your default.")),
        name: String::from(cli.value_of("name").unwrap_or_default()),
        callsign: String::from(cli.value_of("callsign").unwrap_or("Please specify a call!")),
        time: String::from(cli.value_of("time").unwrap()),
        frequency: String::from(cli.value_of("frequency").unwrap_or("Please specify a frequency!")),
    };

    let _db_data = Database::connect(
        // Todo: custom file or Database
        String::from("cq.db")
    );
    let _db = Database {
        file: _db_data.1,
        connection: _db_data.0
    };
    data.write( _db);

}



/*
pub fn fibonatchi(range:usize, display:bool){
    let mut fib1:Vec<u128> = [0,1].to_vec();
    for i in 0..range - 1{
        fib1.push(fib1[i] + fib1[i + 1]);
        if fib1.last().unwrap() % 2 == 0 { print!("even, ") }
        print!("{}, ", fib1.last().unwrap())
    }

}


pub fn test(i:u16) -> u128 {
    return i as u128 * 123;
}

pub fn possibilities(variations:u16, tries:u32, lay_back:bool) -> u128{
    let mut sum:u128 = variations as u128;
    if lay_back {
        return variations.pow(tries) as u128;
    }else {
        for i in 0..tries {
            sum *= (variations - i as u16) as u128;
        }
        return sum;
    }
}
*/
/*
pub fn gui(){
    Window::new("Hello world")
        .size([300.0, 100.0], Condition::FirstUseEver)
        .build(&ui, || {
        ui.text("Hello world!");
    });

}*/