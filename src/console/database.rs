extern crate diesel;
extern crate dotenv;

use console::database::diesel::__diesel_parse_table;
use diesel::table;
use diesel::prelude::*;
use diesel::sqlite::SqliteConnection;

use dotenv::dotenv;

use std::env;
use crate::console::Qso;

pub struct Database{
    pub data: Qso,
}

enum test{

}
impl Database {
    pub fn establish_connection(&self) -> SqliteConnection {
        dotenv().ok();
        println!("{}", &self.data.time);

        let database_url = env::var("DATABASE_URL").expect("Please provide a path.");
        return SqliteConnection::establish(&database_url).expect(&format!("Error connecting to {}", database_url));
    }
    pub fn write(&self){
        // &self.Connection.test_transaction("FROM cq SELECT *;".as_sql());
    }

}


