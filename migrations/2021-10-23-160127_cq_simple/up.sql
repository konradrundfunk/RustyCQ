
CREATE TABLE IF NOT EXISTS "cq" (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    call            TEXT        NOT NULL,
    time            TIMESTAMP   NOT NULL,
    frequency       REAL        NOT NULL,
    mode            TEXT        NOT NULL,
    name            TEXT            NULL

);